var Pasien = require('../models/MPasien')

exports.getAllPasien = async (req, res, next) => {
    try {
        const findUser = await Pasien.findAll();
        res.json({
            status: true,
            message: "Success",
            data: findUser
        })
    } catch (error) {
        res.json({
            status: false,
            message: error,
            data: []
        })
    }
};

exports.getDetailPasien = async (req, res, next) => {
    try {
        var no_rm = req.params.no_rm;
        const findUser = await Pasien.findOne({ where: { no_rm: no_rm } });
        res.json({
            status: true,
            message: "Success",
            data: findUser
        })
    } catch (error) {
        res.json({
            status: false,
            message: error,
            data: []
        })
    }
};

exports.createPasien = async (req, res, next) => {
    try {
        await Pasien.create(req.body).then(data => {
            res.json({
                status: true,
                message: "Success",
                data: data
            })
        })
    } catch (error) {
        res.json({
            status: false,
            message: error,
            data: []
        })
    }
}

exports.updatePasien = async (req, res, next) => {
    try {
        await Pasien.update(req.body, {
            where: {
                no_rm: req.body.no_rm
            }
        }).then(() => {
            res.json({
                status: true,
                message: "Success",
                data: []
            })
        })
    } catch (error) {
        res.json({
            status: false,
            message: error,
            data: []
        })
    }
}

exports.deletePasien = async (req, res, next) => {
    try {
        await Pasien.destroy({
            where: {
                no_rm: req.body.no_rm
            }
        }).then(() => {
            res.json({
                status: true,
                message: "Success",
                data: []
            })
        })
    } catch (error) {
        res.json({
            status: false,
            message: "Failed: " + error,
            data: []
        })
    }
}

exports.getOptions = (req, res, next) => {

    Pasien.findAll().then(async data => {
        var options = [];
        await data.forEach(async (item) => {
            var itemBaru = {
                id: item.no_rm,
                value: item.no_rm + " - " + item.nama
            };
            await options.push(itemBaru);
        });

        res.json({
            status: true,
            message: "Success",
            data: options
        })
    }).catch(error => {
        res.json({
            status: false,
            message: error,
            data: []
        })
    });
};