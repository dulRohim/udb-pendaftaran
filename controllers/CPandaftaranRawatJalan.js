var PendaftaranRawatJalan = require('../models/MPendaftaranRawatJalan')
var Pasien = require('../models/MPasien');
var Poli = require('../models/MPoli');

exports.getAllPendaftaran = async (req, res, next) => {
    await PendaftaranRawatJalan.findAll({ raw: true }).then(async data => {

        await Promise.all(data.map(async (item) => {
            //baca pasien
            const pasien = await Pasien.findByPk(item.no_rm);
            //baca poli
            const poli = await Poli.findByPk(item.id_poli);

            //update itemTampil
            item['nama_pasien'] = pasien.nama;
            item['nama_poli'] = poli.nama;
        }));

        res.json({
            status: true,
            pesan: "Berhasil Tampil",
            data: data
        });
    }).catch(err => {
        res.json({
            status: false,
            pesan: "Gagal tampil: " + err.message,
            data: []
        });
    });
};

exports.getDetailPendaftaran = async (req, res, next) => {
    try {
        var id = req.params.id;
        const model = await PendaftaranRawatJalan.findOne({ where: { id: id } });
        res.json({
            status: true,
            message: "Success",
            data: model
        })
    } catch (error) {
        res.json({
            status: false,
            message: error,
            data: []
        })
    }
};

exports.create = async (req, res, next) => {
    try {
        await PendaftaranRawatJalan.create(req.body).then(data => {
            res.json({
                status: true,
                message: "Success",
                data: data
            })
        })
    } catch (error) {
        res.json({
            status: false,
            message: error,
            data: []
        })
    }
}

exports.update = async (req, res, next) => {
    try {
        var id = req.params.id;
        await PendaftaranRawatJalan.update(req.body, {
            where: {
                id: id
            }
        }).then(() => {
            res.json({
                status: true,
                message: "Success",
                data: []
            })
        })
    } catch (error) {
        res.json({
            status: false,
            message: error,
            data: []
        })
    }
}

exports.delete = async (req, res, next) => {
    try {
        var id = req.params.id;
        await PendaftaranRawatJalan.destroy({
            where: {
                id: id
            }
        }).then(() => {
            res.json({
                status: true,
                message: "Success",
                data: []
            })
        })
    } catch (error) {
        res.json({
            status: false,
            message: "Failed: " + error,
            data: []
        })
    }
}

exports.getOptions = (req, res, next) => {
    PendaftaranRawatJalan.findAll().then(data => {
        var options = data.map(item => {
            return {
                id: item.id,
                value: item.nama
            }
        });
        res.json({
            status: true,
            pesan: "Berhasil Tampil Options",
            data: options
        });
    }).catch(err => {
        res.json({
            status: false,
            pesan: "Gagal tampil: " + err.message,
            data: []
        });
    });
};