// Include Sequelize module.
const Sequelize = require('sequelize');
const connection = require('../connection.js');

const Pasien = connection.define('pasien', {
    no_rm: {
        type: Sequelize.STRING(20),
        allowNull: false,
        primaryKey: true
    },
    nama: {
        type: Sequelize.STRING(100),
        allowNull: false
    },
    alamat: {
        type: Sequelize.STRING(255),
        allowNull: false
    },
    no_telp: {
        type: Sequelize.STRING(15),
        allowNull: false
    },
    created_at: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
    updated_at: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
}, {
    timestamps: false,
})

module.exports = Pasien
