// Include Sequelize module.
const Sequelize = require('sequelize');
const connection = require('../connection.js');

const PendaftaranRawatJalan = connection.define('pendaftaran_rawat_jalan', {
    id: {
        type: Sequelize.INTEGER(10),
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    no_rm: {
        type: Sequelize.STRING(20),
        allowNull: false,
    },
    id_poli: {
        type: Sequelize.INTEGER(10),
        allowNull: false,
    },
    created_at: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
    updated_at: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
}, {
    timestamps: false,
})

module.exports = PendaftaranRawatJalan
