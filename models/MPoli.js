// Include Sequelize module.
const Sequelize = require('sequelize');
const connection = require('../connection_poli');

const Poli = connection.define('poli', {
    id: {
        type: Sequelize.INTEGER(10),
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    nama: {
        type: Sequelize.STRING(100),
        allowNull: false,
    },
    created_at: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
    updated_at: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
}, {
    timestamps: false,
})

module.exports = Poli