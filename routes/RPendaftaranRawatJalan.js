var express = require('express');
var router = express.Router();
var PendaftaranRawatJalan = require('../controllers/CPandaftaranRawatJalan');

router.get('/', PendaftaranRawatJalan.getAllPendaftaran);
router.get('/options', PendaftaranRawatJalan.getOptions);
router.post('/', PendaftaranRawatJalan.create);
router.put('/:id', PendaftaranRawatJalan.update);
router.delete('/:id', PendaftaranRawatJalan.delete);

module.exports = router;