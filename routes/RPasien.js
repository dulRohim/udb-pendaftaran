var express = require('express');
var router = express.Router();
var Pasien = require('../controllers/CPasien');

router.get('/', Pasien.getAllPasien);
router.get('/options', Pasien.getOptions);
router.post('/', Pasien.createPasien);
router.put('/', Pasien.updatePasien);
router.delete('/', Pasien.deletePasien);

module.exports = router;