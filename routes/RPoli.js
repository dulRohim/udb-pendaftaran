var express = require('express');
var router = express.Router();

var Poli = require('../models/MPoli');

router.get('/options', function (req, res, next) {
    Poli.findAll().then(data => {
        var options = data.map(item => {
            return {
                id: item.id,
                value: item.nama
            }
        });
        res.json({
            status: true,
            pesan: "Berhasil Tampil Options",
            data: options
        });
    }).catch(err => {
        res.json({
            status: false,
            pesan: "Gagal tampil: " + err.message,
            data: []
        });
    });
});
module.exports = router;